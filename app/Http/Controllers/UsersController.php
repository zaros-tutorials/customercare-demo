<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function index(Request $request)
    {
        // display all users
        $users = User::paginate(env('PAGINATION_LENGTH'));
        return response()->json($users);
    }

    public function create(Request $request)
    {
        $this->validate($request,
            [
                'username' => 'required|unique:users',
                'email' => 'required|email|unique:users',
                'phone' => 'required|numeric|region|unique:users',
            ], [
                'region' => 'Invalid Phone Number.'
            ]);
        $request['api_token'] = str_random(60);
        $request['password'] = app('hash')->make($request['password']);
        // create new user
            $user = User::create($request->all());
        return response()->json($user);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request,
            [
                'username' => 'required|unique:users,username,' . $id,
                'email' => 'required|email|unique:users,email,' . $id,
                'phone' => 'required|numeric|region|unique:users,phone,' . $id,
            ], [
                'region' => 'Invalid Phone Number.'
            ]);
        //update user
        $user->update($request->all());
        return response()->json($user);
    }

    public function view($id)
    {
        // view user
        $user = User::findOrFail($id);
        return response()->json($user);

    }

    public function destroy(Request $request, $id)
    {
        // delete user
        $user = User::findOrFail($id);
        if ($user->delete()) {
            return response()->json(['success' => TRUE, 'message' => 'User is removed.'], 200);
        }else{
            return response()->json(['success' => FALSE, 'message' => 'User does not removed.'], 401);
        }

    }
}
