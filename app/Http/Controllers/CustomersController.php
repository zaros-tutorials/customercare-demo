<?php

namespace App\Http\Controllers;

use App\Models\Customer;

class CustomersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        // display all users
        $results = Customer::paginate(env('PAGINATION_LENGTH'));

        return response()->json($results);
    }

    public function view($id)
    {
        $customer = Customer::findOrFail($id);

        return response()->json($customer);
    }

    public function viewByHex($hexId)
    {
       // exercise:

    }
}
