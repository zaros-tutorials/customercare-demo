<?php
/**
 * This middleware checks if the authenticated user has
 * the privileges to access a protected admin resource
 *
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class AdminMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->is_admin == 1) {
            return $next($request);
        }

        return response('You don\'t have admin access', 401);
    }
}
