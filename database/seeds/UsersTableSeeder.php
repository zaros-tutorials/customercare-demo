<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            [
                'api_token' => 'adminuser',
                'username'  => 'admin',
                'is_admin'  => 1,
                'is_active' => 1,
            ],
            [
                'api_token' => 'apiuser',
                'username'  => 'sc_service',
                'is_admin'  => 0,
                'is_active' => 1,
            ],
            [
                'api_token' => 'disableduser',
                'username'  => 'elvis',
                'is_admin'  => 0,
                'is_active' => 0,
            ],
            [
                'api_token' => 'disabledadminuser',
                'username'  => 'hawking',
                'is_admin'  => 1,
                'is_active' => 0,
            ],
        ]);
    }
}
