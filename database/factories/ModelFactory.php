<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username'  => $faker->name,
        'email'     => $faker->email,
        'api_token' => str_random(32),
        'password'  => $faker->password,
        'phone'     => $faker->phoneNumber,
        'is_active' => $faker->boolean,
        'is_admin'  => $faker->boolean,
    ];
});

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username'  => $faker->name,
        'email'     => $faker->email,
        'api_token' => str_random(32),
        'password'  => $faker->password,
        'phone'     => $faker->phoneNumber,
        'is_active' => 1,
        'is_admin'  => 0,
    ];
});

$factory->define(App\Models\Customer::class, function (Faker\Generator $faker) {
    return [
        'name'       => $faker->name,
        'ref_id'     => $faker->uuid,
        'email'      => $faker->email,
        'phone'      => $faker->phoneNumber,
        'address'    => $faker->address,
        'notes'      => '',
        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
    ];
});
