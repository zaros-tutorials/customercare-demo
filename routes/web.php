<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'admin/', 'middleware' => ['auth', 'admin', 'cors']], function ($router) {
    //users
    $router->group(['prefix' => 'users'], function ($router) {
        $router->post('create', 'UsersController@create');

        $router->get('view/{id}', 'UsersController@view');

        $router->put('update/{id}', 'UsersController@update');

        $router->delete('destroy/{id}', 'UsersController@destroy');

        $router->get('index', 'UsersController@index');
    });

    // customers
    $router->group(['prefix' => 'customers'], function ($router) {
        $router->get('index', 'CustomersController@index');
    });
});

$router->group(['prefix' => 'api/', 'middleware' => ['auth', 'cors']], function ($router) {
    // customers
    $router->group(['prefix' => 'customers'], function ($router) {
        $router->get('index', 'CustomersController@index');
        $router->get('view/{id}', 'CustomersController@view');
        $router->get('viewByHex/{id}', 'CustomersController@view');
    });

});
