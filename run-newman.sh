#!bin/bash
# Runs newman tests and generates a HTML report

newman run postman/collection.json \
     --globals postman/globals.json \
     --insecure \
     --reporters cli,html --reporter-html-export newman-report.html