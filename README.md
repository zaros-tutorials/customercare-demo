[![pipeline status](https://gitlab.com/zaros-tutorials/customercare-demo/badges/master/pipeline.svg)](https://gitlab.com/zaros-tutorials/customercare-demo/commits/master) [![coverage report](https://gitlab.com/zaros-tutorials/customercare-demo/badges/master/coverage.svg)](https://gitlab.com/zaros-tutorials/customercare-demo/commits/master)
#  CustomerCare Demo App

### Preparation

- [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Install Docker Community Edition in your machine. [https://docs.docker.com/install/](https://docs.docker.com/install/)
- Create your account on [Docker Hub](https://hub.docker.com) 
- Login into Docker Community Edition with your new credentials.

### Installing and Starting

- Clone this repository  
- If your repo is not in docker file sharing make sure you add it now.
    You can do so by going to preferences of Docker Community Edition.
    
    <img width="400" src="https://i.imgur.com/Tve7DW3.png" />

- Setup your environment files:
    ```$xslt
    $ cp .env.example .env
    $ cp .env.testing.example .env.testing
    ```

- Run `docker-compose up`
- Go to [https://localhost/](https://localhost)

### Database Migration and Seeding

If you are setting up for the first time, Docker will create a new database stored in `storage/db/` (or an alternative location which you have specified in your `docker-compose.yml`).

Next, you'll need to initialise the database using `artisan migrate`. This is how to do it from outside the Docker container.

```bash
$ docker exec demo_app php artisan migrate --seed
```

### Stopping the Docker Network

```bash
$ docker-compose down
```

## Command Line commands

If you need to update composer or do anything on command line, you need to do it from inside the docker container.

```bash 
$ docker exec -t -i demo_app bash
```

alternatively,

```bash 
$ docker-compose exec app bash
```


## Testing

Shell into the app container and run PHPUnit

```bash 
$ docker exec -t -i demo_app bash

# in the container
$ cd /var/www
$ vendor/bin/phpunit
```

Alternatively, executing  phpunit using docker works too!
```bash 
$ docker exec demo_app vendor/bin/phpunit
```

## Postman / Newman

Newman tests are automatically run when new commits are pushed to Git.

You can use the command line to perform the tests manually.  There are some files for the Postman Collection and Environment in the `postman` directory which are used to run these tests.

Once you have [installed Newman](https://www.getpostman.com/docs/v6/postman/collection_runs/command_line_integration_with_newman#getting-started), you can run this script, which will execute `newman` with the necessary options:

```bash
$ sh run-newman.sh
```

The generated test report can be viewed in `newman-report.html`.

Alternatively, of course, you can [download Postman](https://www.getpostman.com/apps) to import the files and run tests from the app.

## API Documentation

The documentation generated by Postman is available at:

https://documenter.getpostman.com/view/4556048/RWEcQMAu

