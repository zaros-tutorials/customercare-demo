#!/bin/bash

# exit script if any command fails (non-zero value)
set -e

if [ ! -f /var/www/.env ]; then
    cp /var/www/.env.example .env
fi

if [ ! -f /var/www/.env.testing ]; then
    cp /var/www/.env.testing.example .env.testing
fi

chmod -R 0755 /var/www/githooks/

cp -rp /var/www/githooks /var/www/.git/hooks

cd /var/www
composer update

# start the cronjob for Task Scheduler
service cron start

# run database migration (disabled)
# php artisan migrate

exec php-fpm

# https://stackoverflow.com/questions/39082768/what-does-set-e-and-exec-do-for-docker-entrypoint-scripts
exec "$@"
