<?php

/**
 * Tests the AdminMiddleware
 *
 * Thanks to Christopher Vundi
 *
 * @link https://semaphoreci.com/community/tutorials/testing-middleware-in-laravel-with-phpunit
 */

namespace Tests;

use App\Http\Middleware\AdminMiddleware;
use Illuminate\Http\Request;
use App\Models\User;
use TestCase;

class AdminMiddlewareTest extends TestCase
{
    /**
     * Non Admins cannot pass
     */
    public function testNonAdminsCannotPass()
    {
        $user = factory(User::class)->make(['is_admin' => false]);

        $this->actingAs($user);

        $request = Request::create('/admin', 'GET');

        $middleware = new AdminMiddleware;

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals($response->getStatusCode(), 401);
    }


    /**
     * Admins can pass
     */
    public function testAdminsCanPass()
    {
        $user = factory(User::class)->make(['is_admin' => true]);

        $this->actingAs($user);

        $request = Request::create('/admin', 'GET');

        $middleware = new AdminMiddleware;
        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals($response, null);
    }
}
