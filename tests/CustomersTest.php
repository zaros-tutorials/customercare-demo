<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;

class CustomersTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->app->instance('middleware.disable', true);

        // migrate and seed database
        $this->artisan('migrate');
        $this->artisan('db:seed', ['--class' => 'DatabaseSeeder']);
    }

    public function testListCustomers()
    {
        $this->get('/api/customers/index');
        $this->assertResponseOK();
    }

    /**
     * Must provide an ID for a customer
     */
    public function testViewCustomerWithNoId()
    {
        $this->get('/api/customers/view');
        $this->assertResponseStatus(404);
    }

    /**
     * View customer with a valid id
     */
    public function testViewCustomerWithId()
    {
        $this->get('/api/customers/view/1');
        $this->assertResponseStatus(200);
    }

    /**
     * Valid customer ID is an integer
     */
    public function testViewCustomerWithInvalidId()
    {
        $this->get('/api/customers/view/astring');
        $this->assertResponseStatus(404);
    }


    /**
     * Exercise: can retrieve a customer with a string that is a hexadecimal (base-16)
     */
    public function testViewCustomerWithHexId()
    {
        $this->get('/api/customers/viewByHex/64');
        $this->assertResponseStatus(200);
    }
}