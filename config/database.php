<?php

return [
    'default'     => 'default',
    'migrations'  => 'migrations',
    'connections' => [
        'default' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB_DATABASE'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'testing' => [
            'driver'    => 'mysql',
            'host'      => env('DBT_HOST'),
            'port'      => env('DBT_PORT'),
            'database'  => env('DBT_DATABASE'),
            'username'  => env('DBT_USERNAME'),
            'password'  => env('DBT_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
    ],
];
